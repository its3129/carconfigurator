# Rozalina's Car configurator backend

- Rozalina's Car Configurator front-end can be found here: https://gitlab.com/its3129/carconfiguratorfrontend

- Documentation for the whole project  can be find in "DOCUMENTATION.md" in the current repository

- Project managment of the whole project can be found in this repository -> Issues (on the left, GitLab Boards)
