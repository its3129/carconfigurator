# PROJECT DOCUMENTATION
This file contains all the relevant documentation for the project Car Configurator. Next to the title of the document a OneDrive link will be provided leading to a folder containing all of its versions. The links to the folders have been shared with both teahcers.

## Cultural Awareness Report 
https://stichtingfontys-my.sharepoint.com/:f:/r/personal/384613_student_fontys_nl/Documents/Car%20Configuraotor%20Documentation/Cultural%20Awareness%20Report?csf=1&web=1&e=WNAcEe

## OWASP Security Report 
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/EtjypyUmgVJDmmaPYIuUp5gBlMS6PXQ55BVy9U86O5JXGA?e=HU9NkU

## Desgin Document 
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/ErtlMyssx2ZJqc3z1E08aXoBzrb5va2y5LvEkzkAcYKdgg?e=taleSK 

## Research Document 
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/EheUG3kfzjZJm76cczyOJZEBUsJMyw8pWDiQf4_lmawbAQ?e=BVdoIM

## Test Plan
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/Eo9bhetuf4pEgPsuDHvO3qwB0ALMD0_5bH5XPDGimi24Qw?e=7PdhrJ

## Second Iteration Prototype
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/ElqI5mI1sS5OkBta2m_0tHoBeysOosLxTllg2dlL7ytRJw?e=QHp5Xl

## Planning Demonstration Videos
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/EvGebeKNX-FOjWk8gNifwzIB_sCuo5ZXpm9MuYNyIje18A?e=sy8ngZ

## Sonarqube screenshot/report pre-post -> Quality assurance metrics
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/EnsDsmhMk4pKuAqFGAUsHOcBQC9Zioiqynmx8KtgBSCk7A?e=gRaWeZ

## First release version (demonstrating authentication/authorization integration)
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/ErxcRKKCYmxMoj_7tfROO7oBJ9fJwkg-m8N82pz8g7YMtA?e=RWf76m

## UX feedback report (feedback from 2 different users) and resulting improvements on your UI
https://stichtingfontys-my.sharepoint.com/:f:/g/personal/384613_student_fontys_nl/Eoq4ZYOEpDZCkp6jJuhBki8BzbQRqw4zpJ6vT-MLKLjRrw?e=MdJeGX
