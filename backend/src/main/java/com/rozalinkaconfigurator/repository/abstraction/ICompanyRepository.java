package com.rozalinkaconfigurator.repository.abstraction;


import com.rozalinkaconfigurator.model.Company;

import java.util.List;

public interface ICompanyRepository extends IRepository<Company, Long> {
    List<Company> findAll();
    boolean exists(Long id);
}
