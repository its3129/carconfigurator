package com.rozalinkaconfigurator.repository.abstraction;

import com.rozalinkaconfigurator.model.Department;

import java.util.List;

public interface IDepartmentRepository extends IRepository<Department, Long> {
    List<Department> findAll();
    Department findByName(String name);
}
