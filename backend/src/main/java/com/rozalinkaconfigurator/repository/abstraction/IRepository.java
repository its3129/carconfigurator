package com.rozalinkaconfigurator.repository.abstraction;

public interface IRepository<T, S> {
    public boolean create(T t);
    public T read(S s);
    public boolean update(T t);
    public boolean delete(S s);

}
