package com.rozalinkaconfigurator.repository.impl.jpa;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.model.Department;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.repository.abstraction.IDepartmentRepository;
import com.rozalinkaconfigurator.service.abstraction.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Qualifier("JpaDepartmentRepository")
@Transactional
public class JPADepartmentRepository implements IDepartmentRepository {

    @PersistenceContext
    private EntityManager manager;

    @Autowired
    private JPADepartmentRepositoryDAO dao;

    @Autowired
    private JPACompanyRepositoryDAO companyRepositoryDAO;

    @Override
    public boolean create(Department department) {
        manager.persist(department);
        return  true;
    }

    @Override
    public Department read(Long aLong) {
        Department department = dao.findById(aLong).orElseThrow(() -> new IllegalArgumentException(("No department exists with id:")));
        department.setCompany(companyRepositoryDAO.getOne(department.getCompany().getId()));
        return department;
    }

    @Override
    public boolean update(Department department) {
        Department nnn = dao.findById(department.getId()).orElseThrow();
        nnn.setName(department.getName());
        nnn.setCompany(department.getCompany());

        dao.saveAndFlush(nnn);
        return true;
    }

    @Override
    public boolean delete(Long aLong) {
        manager.remove(this.read(aLong));
        return  true;
    }

    @Override
    public List<Department> findAll() {
        return dao.findAll();
    }

    @Override
    public Department findByName(String name) {
        return null;
    }
}
