package com.rozalinkaconfigurator.repository.impl.jpa;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
@Component
@Qualifier("JpaCompanyRepository")
@Transactional
public class JPACompanyRepository implements ICompanyRepository {

    @Autowired
    private JPACompanyRepositoryDAO dao;

    @PersistenceContext
    private EntityManager manager;


    @Override
    public boolean create(Company company) {
        manager.persist(company);
        return true;
    }

    @Override
    public Company read(Long aLong) {
       // return dao.findById(aLong).orElse(null);
        return dao.findById(aLong).orElseThrow(() -> new IllegalArgumentException("No company exists with id: "));
    }

    @Override
    public boolean update(Company company) {
        Company nnn = dao.findById(company.getId()).orElseThrow();
        nnn.setName(company.getName());
        nnn.setAddressStreet(company.getAddressStreet());
        nnn.setAddressNumber(company.getAddressNumber());
        nnn.setAddressPostfix(company.getAddressPostfix());
        nnn.setPostalCodeNumbers(company.getPostalCodeNumbers());
        nnn.setPostalCodeLetters(company.getPostalCodeLetters());
        nnn.setImageUrl(company.getImageUrl());

        dao.saveAndFlush(nnn);
        return true ;
    }

    @Override
    public boolean delete(Long aLong) {
         manager.remove(this.read(aLong));
         return true;
    }

    @Override
    public List<Company> findAll() {
        return dao.findAll();
        //return manager.createNativeQuery("SELECT * FROM Companies", Company.class).getResultList();
    }

    @Override
    public boolean exists(Long id) {
        return dao.existsById(id);
    }
}
