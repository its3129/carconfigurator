package com.rozalinkaconfigurator.repository.impl.fake;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.model.Department;
import com.rozalinkaconfigurator.repository.abstraction.IDepartmentRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FakeDepartmentRepository implements IDepartmentRepository {

    private final List<Department> departments;
    public FakeDepartmentRepository() {
        departments = new ArrayList<Department>();
        Department department = new Department();
        department.setName("Department1");
        department.setId(2L);
        department.setCreatedAt(new Date());
    }

    @Override
    public boolean create(Department department) {
        this.departments.add(department);
        return true;
    }

    @Override
    public Department read(Long aLong) {
        return departments.stream().filter(d -> Objects.equals(d.getId(), aLong)).collect(Collectors.toList()).stream().findFirst().orElseThrow();
    }

    @Override
    public boolean update(Department department) {
        Department departmentFromDb = read(department.getId());
        departmentFromDb.setName(department.getName());
        return true;
    }

    @Override
    public boolean delete(Long aLong) {
        Department department1 = read(aLong);
        return departments.removeIf(n -> Objects.equals(department1.getId(), aLong));
    }

    @Override
    public List<Department> findAll() {
        return departments;
    }

    @Override
    public Department findByName(String name) {
        return null;
    }
}
