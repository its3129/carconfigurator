package com.rozalinkaconfigurator.repository.impl.fake;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Qualifier("companyFakeDataRepository")
public class FakeCompanyRepository implements ICompanyRepository {
    private final List<Company> companies;
    public FakeCompanyRepository() {
        companies = new ArrayList<Company>();
    }

    @Override
    public boolean create(Company company) {
        this.companies.add(company);
        return true;
    }

    @Override
    public Company read(Long aLong) {
        return companies.stream().filter(d -> Objects.equals(d.getId(), aLong)).collect(Collectors.toList()).stream().findFirst().orElseThrow();
    }

    @Override
    public boolean update(Company company) {
        Company companyFromDb = read(company.getId());
        //return  companies.remove(companyFromDb);
        companyFromDb.setName(company.getName());
        companyFromDb.setImageUrl(company.getImageUrl());
        companyFromDb.setPostalCodeLetters(company.getPostalCodeLetters());
        companyFromDb.setAddressNumber(company.getAddressNumber());
        companyFromDb.setAddressPostfix(company.getAddressPostfix());
        companyFromDb.setAddressStreet(company.getAddressStreet());
        //return companies.add(companyFromDb);
        return true;
    }

    @Override
    public boolean delete(Long aLong) {
       // Company ee = companies.stream().filter(c -> Objects.equals(c.getId(), aLong))
                //collect(Collectors.toList()).stream().findFirst().orElseThrow();
        Company company1 = read(aLong);
        return companies.removeIf(n -> Objects.equals(company1.getId(), aLong));
//        return  true;
       // this.companies.remove(ee);
    }

    @Override
    public List<Company> findAll() {
        return companies;
    }

    @Override
    public boolean exists(Long id) {
        return false;
    }
}
