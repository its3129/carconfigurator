package com.rozalinkaconfigurator.repository.impl.jpa;

import com.rozalinkaconfigurator.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JPACompanyRepositoryDAO extends JpaRepository<Company, Long> {

}

