package com.rozalinkaconfigurator.repository.impl.jpa;

import com.rozalinkaconfigurator.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JPADepartmentRepositoryDAO extends JpaRepository<Department, Long> {
}
