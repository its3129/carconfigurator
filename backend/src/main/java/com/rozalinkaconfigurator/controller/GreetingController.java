package com.rozalinkaconfigurator.controller;

import com.rozalinkaconfigurator.model.Greeting;
import com.rozalinkaconfigurator.model.NotificationDTO;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GreetingController {

    @MessageMapping("/notifications")
    @SendTo("/topic/notifications")
    public Greeting greeting(NotificationDTO message) throws Exception {
        return new Greeting( HtmlUtils.htmlEscape(message.getMessage()));
    }

}
