package com.rozalinkaconfigurator.controller;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private ICompanyService companyService;

    @GetMapping()
    public List<Company> getAllCompanies() {
        return this.companyService.findAll();
    }

    @PostMapping()
    public boolean AddCompany(@RequestBody Company company) throws NotFoundException {
        System.out.println(company);
        return this.companyService.create(company);
    }

    @GetMapping("/{id}")
    public Company getCompanyByID(@PathVariable(value="id") long id) {
        return this.companyService.read(id);
    }

    @DeleteMapping("/{id}")
    public  boolean deleteCompany(@PathVariable(value="id") int id){
        return this.companyService.delete((long)id);
    }

    @PutMapping()
    public boolean updateCompany(@RequestBody Company company){
    return this.companyService.update(company);
    }
}
