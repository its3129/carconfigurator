package com.rozalinkaconfigurator.controller;

import com.rozalinkaconfigurator.dto.department.CreateDepartmentDTO;
import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.model.Department;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import com.rozalinkaconfigurator.service.abstraction.IDepartmentService;
import javassist.NotFoundException;
import net.bytebuddy.jar.asm.commons.Remapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private ICompanyService companyService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping()
    public List<Department> getAllDepartments() { return this.departmentService.findAll();}


    @GetMapping("/{id}")
    public  Department getDepartmentById(@PathVariable(value = "id") long id) {

        return this.departmentService.read(id);
    }

    @DeleteMapping("/{id}")
    public boolean deleteDepartmentById(@PathVariable(value = "id") long id) {return  this.departmentService.delete(id);}

    @PutMapping()
    public boolean updateDepartment(@RequestBody Department department) { return this.departmentService.update(department);}

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public boolean createDepartment(@RequestBody CreateDepartmentDTO postDTO) throws NotFoundException {
        Department department = convertToEntity(postDTO);
        Company company = companyService.read(postDTO.getId());
        department.setCompany(company);
        return departmentService.create(department);
    }


    private Department convertToEntity(CreateDepartmentDTO postDTO) {
        Department department = new Department();
        department.setName(postDTO.getName());
        return department;
    }
}
