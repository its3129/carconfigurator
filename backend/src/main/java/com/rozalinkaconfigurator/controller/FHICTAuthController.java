package com.rozalinkaconfigurator.controller;

import com.google.gson.Gson;
import com.rozalinkaconfigurator.dto.Oauth2.fhict.Oauth2Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/callback")
public class FHICTAuthController {
    @Value("${secret}")
    private String oauthSecret;

    @GetMapping()
    public void retrieveToken(@RequestParam String code, HttpServletResponse httpServletResponse) {
        OkHttpClient client = new OkHttpClient();

        FormBody formBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("code", code)
                .add("redirect_uri", "http://localhost:3005/callback")
                .add("client_id", "i384613-bluecableb")
                .add("client_secret", oauthSecret)
                .add("scope", "fhict fhict_personal")
                .build();
        Request request = new Request.Builder()
                .url("https://identity.fhict.nl/connect/token")
                .post(formBody)
                .build();

        try (ResponseBody res = client.newCall(request).execute().body()) {
            Gson gson = new Gson();
            Oauth2Callback callback = gson.fromJson(res.string(), Oauth2Callback.class);

            httpServletResponse.setHeader("Location", String.format("http://localhost:3000?accessToken=%s&refreshToken=%s", callback.access_token, callback.refresh_token));
            httpServletResponse.setStatus(302);
            httpServletResponse.sendRedirect(String.format("http://localhost:3000/callback?accessToken=%s&refreshToken=%s", callback.access_token, callback.refresh_token));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
