package com.rozalinkaconfigurator.dto.Oauth2.fhict;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
public class Oauth2Callback {
    public String access_token;
    public String token_type;
    public int expires_in;
    public String refresh_token;
}