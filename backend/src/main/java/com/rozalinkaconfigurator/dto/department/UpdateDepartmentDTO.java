package com.rozalinkaconfigurator.dto.department;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UpdateDepartmentDTO {
    @NotEmpty
    Long id;

    @NotEmpty
    @Size(min = 5, max = 2000)
    String name;
}
