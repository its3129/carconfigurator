package com.rozalinkaconfigurator.dto.department;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter @Setter
public class CreateDepartmentDTO {
    /**
     * Denotes the company id.
     */
    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    private String name;

}
