package com.rozalinkaconfigurator.model;

public class NotificationDTO {

    private String message;

    public NotificationDTO() {
    }

    public NotificationDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
