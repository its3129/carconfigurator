package com.rozalinkaconfigurator.service.abstraction;

import javassist.NotFoundException;

public interface IService<T> {
    boolean create(T t) throws NotFoundException;
    T read(Long id);
    boolean update(T t);
    boolean delete(Long id);
}
