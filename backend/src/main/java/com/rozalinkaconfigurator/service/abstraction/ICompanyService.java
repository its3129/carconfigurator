package com.rozalinkaconfigurator.service.abstraction;

import com.rozalinkaconfigurator.model.Company;

import java.util.List;

public interface ICompanyService extends IService<Company> {
    List<Company> findAll();
    boolean exists(Long id);

}
