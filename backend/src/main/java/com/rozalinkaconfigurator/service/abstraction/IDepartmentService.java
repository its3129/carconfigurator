package com.rozalinkaconfigurator.service.abstraction;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.model.Department;

import java.util.List;

public interface IDepartmentService extends IService<Department> {
    List<Department> findAll();
    Department readByName(String name);
}
