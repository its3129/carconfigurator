package com.rozalinkaconfigurator.service.impl;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CompanyService implements ICompanyService {

    @Autowired
    private ICompanyRepository companyRepository;

    @SneakyThrows
    @Override
    public boolean create(Company company)  {
        if (company.getName().length() > 50) {
            throw new Exception("Name cannot be more than 50 characters");
        }
        Company companyPersist = new Company();

        companyPersist.setName(company.getName());
        companyPersist.setAddressNumber(company.getAddressNumber());
        companyPersist.setPostalCodeLetters(company.getPostalCodeLetters());
        companyPersist.setPostalCodeNumbers(company.getPostalCodeNumbers());
        companyPersist.setImageUrl(company.getImageUrl());
        return companyRepository.create(companyPersist);
    }

    @Override
    public Company read(Long id) {
        return companyRepository.read(id);
    }

    @Override
    @SneakyThrows
    public boolean update(Company company) {
        if (company.getName().length() > 50) {
            throw new Exception("Name cannot be more than 50 characters");
        }
        if (company.getPostalCodeLetters().length() > 2) {
            throw new Exception("Postal code letters cannot be longer than 2");
        }
        if (company.getAddressPostfix().length() > 5) {
            throw new Exception("Postfix can not be longer than 5 characters");
        }
        return companyRepository.update(company);
    }

    @Override
    public boolean delete(Long id) {
        return companyRepository.delete(id);
    }

    @Override
    public List<Company> findAll() {
       return this.companyRepository.findAll();
    }

    @Override
    public boolean exists(Long id) {
        return companyRepository.exists(id);
    }
}
