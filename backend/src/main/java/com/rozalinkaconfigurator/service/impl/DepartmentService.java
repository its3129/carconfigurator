package com.rozalinkaconfigurator.service.impl;

import com.rozalinkaconfigurator.model.Department;
import com.rozalinkaconfigurator.repository.abstraction.IDepartmentRepository;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import com.rozalinkaconfigurator.service.abstraction.IDepartmentService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService implements IDepartmentService  {

    @Autowired
    private IDepartmentRepository departmentRepository;

    @Autowired
    private ICompanyService companyService;

    @Override
    public boolean create(Department department) throws NotFoundException {
        if (!companyService.exists(department.getCompany().getId())) {
            throw new NotFoundException("Company not found");
        }
        return departmentRepository.create(department);
    }

    @Override
    public Department read(Long id) {
        return departmentRepository.read(id);
    }

    @Override
    public boolean update(Department department) {
        return departmentRepository.update(department);
    }

    @Override
    public boolean delete(Long id) {
        return departmentRepository.delete(id);
    }

    @Override
    public List<Department> findAll() {
        return this.departmentRepository.findAll();
    }

    @Override
    public Department readByName(String name) {
        return null;
    }
}
