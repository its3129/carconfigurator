package com.rozalinkaconfigurator;

import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.repository.abstraction.IDepartmentRepository;
import com.rozalinkaconfigurator.repository.impl.fake.FakeCompanyRepository;
import com.rozalinkaconfigurator.repository.impl.jpa.JPACompanyRepository;
import com.rozalinkaconfigurator.repository.impl.jpa.JPADepartmentRepository;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import com.rozalinkaconfigurator.service.abstraction.IDepartmentService;
import com.rozalinkaconfigurator.service.impl.CompanyService;
import com.rozalinkaconfigurator.service.impl.DepartmentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@SpringBootApplication
public class RozalinkaApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(RozalinkaApiApplication.class, args);
    }
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
                @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*").allowedHeaders("*").allowedMethods("*");
            }
        };
    }


    @Bean
    public ICompanyRepository companyRepository() {
        return new JPACompanyRepository();
    }

    public IDepartmentRepository departmentRepository() {return new JPADepartmentRepository(); }
//    private final IDepartmentRepository deparmentRepository = null;
}