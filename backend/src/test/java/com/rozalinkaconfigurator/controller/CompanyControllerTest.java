package com.rozalinkaconfigurator.controller;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class CompanyControllerTest {

    @InjectMocks
    CompanyController controller;

    @Mock
    ICompanyService companyService;

    @Mock
    Company company;

    @Mock
    List<Company> companies;

    CompanyControllerTest() {
        controller = new CompanyController();
    }

    @BeforeEach
    void setUp(){
        this.companyService = Mockito.mock(ICompanyService.class);
        this.controller = new CompanyController();
        this.company = new Company();
        companies.add(company);
    }
    @Test
    void getAllCompanies() {
    }

    @Test
    void getCompanyByID() {
    }

    @Test
    void deleteCompany() {
    }

    @Test
    void updateCompany() {
    }
}