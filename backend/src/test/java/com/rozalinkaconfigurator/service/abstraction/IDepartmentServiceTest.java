package com.rozalinkaconfigurator.service.abstraction;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.model.Department;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.repository.abstraction.IDepartmentRepository;
import com.rozalinkaconfigurator.repository.impl.fake.FakeCompanyRepository;
import com.rozalinkaconfigurator.repository.impl.fake.FakeDepartmentRepository;
import com.rozalinkaconfigurator.service.impl.CompanyService;
import com.rozalinkaconfigurator.service.impl.DepartmentService;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class IDepartmentServiceTest {
    @TestConfiguration
    public static class OverrideBean {
        @Bean
        IDepartmentService departmentService() {
            return new DepartmentService();
        }
        @Bean
        @Primary
        IDepartmentRepository departmentRepository() {
            return new FakeDepartmentRepository();
        }
    }

    @Autowired
    IDepartmentService departmentService;

    @Autowired
    IDepartmentRepository departmentRepository;

    @BeforeEach //instead of @Before have to use @BeforeEach in junit5
    public void setup() {
        Department department = new Department();
        department.setName("DepTest");
        department.setId(1L);
        Exception thrown = assertThrows(
                Exception.class,
                () -> this.departmentService.create(department),
                "Name cannot be more than 50 characters"
        );
        departmentRepository.create(department);
    }

    @Test
    void read_department_exists() {
        Department department = new Department();
        department.setId(1L);
        Department departmentRead = departmentService.read(1L);
        assertEquals(department.getId(), departmentRead.getId());
    }

    @Test
    void update_department_exists() {
        Department departmentRead = departmentService.read(1L);

        Department department = new Department();
        department.setName("Gosho");
        department.setId(1L);

        //UPDATE THE DEPARTMENT RETRIEVED EARLIER.
        assertTrue(departmentService.update(departmentRead));
        assertEquals(department.getName(), "Gosho");

    }

    void update_department_that_does_not_exist_throws_error() {
        Department department = new Department();
        department.setId(4000L);
        Exception thrown = assertThrows(
                Exception.class,
                () -> departmentService.update(department),
                "NoSuchElement"
        );
    }

    @Test
    void delete_department_that_does_not_exist_throws_error() {
        Exception thrown = assertThrows(
                Exception.class,
                () -> departmentService.delete(700000L),
                "NoSuchElement"
        );
    }

    @Test
    void findAll() {
        //assertEquals(service.findAll(), departments);
        List<Department> departments = departmentService.findAll();
        assertEquals(departments, departmentService.findAll());
    }

}