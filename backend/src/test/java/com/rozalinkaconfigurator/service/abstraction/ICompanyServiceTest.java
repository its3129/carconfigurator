package com.rozalinkaconfigurator.service.abstraction;

import com.rozalinkaconfigurator.model.Company;
import com.rozalinkaconfigurator.repository.abstraction.ICompanyRepository;
import com.rozalinkaconfigurator.repository.impl.fake.FakeCompanyRepository;
import com.rozalinkaconfigurator.service.abstraction.ICompanyService;
import com.rozalinkaconfigurator.service.impl.CompanyService;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.jpa.domain.AbstractPersistable_.id;


//@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true", webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {com.rozalinkaconfigurator.RozalinkaApiApplication.class})
@SpringBootConfiguration()
@ContextConfiguration(classes = ICompanyServiceTest.OverrideBean.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class ICompanyServiceTest {

    @TestConfiguration
    public static class OverrideBean {
        @Bean
        ICompanyService companyService() {
            return new CompanyService();
        }
        @Bean
        @Primary
        ICompanyRepository companyRepository() {
            return new FakeCompanyRepository();
        }
    }

    @Autowired
    ICompanyService companyService;

    @Autowired
    ICompanyRepository companyRepository;

    @BeforeEach //instead of @Before have to use @BeforeEach in junit5
    public void setup() {
        Company company = new Company();
        company.setName("Test");
        company.setId(1L);
        company.setAddressStreet("Jan van Riebeecklan");
        company.setAddressNumber(72);
        company.setAddressPostfix("B");
        company.setPostalCodeNumbers(5642);
        company.setPostalCodeLetters("MD");
        company.setImageUrl("http://test.com");
        company.setCreatedAt(new Date());
        companyRepository.create(company);

    }

    @Test
    void delete_company_exists() {
        Company company = new Company();
        assertTrue(companyService.delete(1L));
        int lengthAfterDelete = this.companyService.findAll().size();

        assertEquals(0, lengthAfterDelete);
    }


    @Test
    void create_company_does_not_exist() throws NotFoundException {
        Company company = new Company();
        company.setName("Test");
        company.setId(2L);
        company.setImageUrl("http://test.com");
        assertTrue(this.companyService.create(company));
        assertEquals(company.getName(), "Test");
    }

    @Test
    void create_company_name_more_than_50_chars_trows_error() {
        Company company = new Company();
        company.setName("012345678901234567890123456789012345678901aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        company.setId(1L);
        company.setImageUrl("http://test.com");
        Exception thrown = assertThrows(
                Exception.class,
                () -> this.companyService.create(company),
                "Name cannot be more than 50 characters"
        );
    }

    @Test
    void read_company_exists() {
        Company company = new Company();
        company.setId(1L);
        Company companyRead = companyService.read(1L);
        assertEquals(company.getId(), companyRead.getId());
    }

    @Test
    void update_company_exists() {
        Company companyRead = companyService.read(1L);

        Company company = new Company();
        company.setName("Gosho");
        company.setId(1L);
        company.setAddressStreet("Jan van Riebeecklan");
        company.setAddressNumber(72);
        company.setAddressPostfix("B");
        company.setPostalCodeNumbers(5642);
        company.setPostalCodeLetters("MD");
        company.setImageUrl("http://test.com");
        company.setCreatedAt(new Date());

        //UPDATE THE COMPANY RETRIEVED EARLIER.
        assertTrue(companyService.update(companyRead));
        assertEquals(company.getName(), "Gosho");
        assertEquals(company.getAddressStreet(),"Jan van Riebeecklan");
        assertEquals(company.getAddressNumber(),72);
        assertEquals(company.getPostalCodeNumbers(),5642);
        assertEquals(company.getPostalCodeLetters(), "MD");
        assertEquals(company.getImageUrl(),"http://test.com" );
        //assertEquals(companyRead.getName(), companyRead.getName());
    }

    @Test
    void update_company_that_does_not_exist_throws_error() {
        Company company = new Company();
        company.setId(4000L);
        Exception thrown = assertThrows(
                Exception.class,
                () -> companyService.update(company),
                "NoSuchElement"
        );
    }

    @Test
    void delete_company_that_does_not_exist_throws_error() {
        Exception thrown = assertThrows(
                Exception.class,
                () -> companyService.delete(700000L),
                "NoSuchElement"
        );
    }

    @Test
    void findAll() {
        //assertEquals(service.findAll(), companies);
        List<Company> companies = companyService.findAll();
        assertEquals(companies, companyService.findAll());
    }
}
