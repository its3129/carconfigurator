import axios from 'axios'

export class DepartmentService {
  async getDepartments() {
    const result = await axios.get('http://localhost:3005/departments')
    return result.data
  }

  async getDepartment(id) {
    const result = await axios.get(`http://localhost:3005/departments/${id}`)
    return result.data
  }

  async deleteDepartment(id) {
    const result = await axios.delete(`http://localhost:3005/departments/${id}`)
    return result.data
  }
}
