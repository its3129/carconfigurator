export class AuthService {
  async setTokens(accessToken, refreshToken) {
    localStorage.setItem('access_token', accessToken)
    localStorage.setItem('refresh_token', refreshToken)
  }

  getAccessToken() {
    return localStorage.getItem('access_token')
  }

  async getRefreshToken() {
    return localStorage.getItem('refresh_token')
  }
}
