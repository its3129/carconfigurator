import axios from 'axios'

export class CompanyService {
  async getCompanies() {
    const result = await axios.get('http://localhost:3005/companies')
    return result.data
  }

  async getCompany(id) {
    const result = await axios.get(`http://localhost:3005/companies/${id}`)
    return result.data
  }

  async addCompany(name, streetName, postalCodeNumbers, postalCodeLetters, copmanyImg) {
    const result = await axios.post(`http://localhost:3005/companies/`)
    return result.data
  }

  async deleteCompany(id) {
    const result = await axios.delete(`http://localhost:3005/companies/${id}`)
    return result.data
  }
}
