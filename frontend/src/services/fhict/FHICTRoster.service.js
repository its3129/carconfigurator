import { AuthService } from '../Auth.service'
import * as axios from 'axios'
export class FHICTRosterService {
  authService = new AuthService()

  async getRoster() {
    const request = await axios.default.get(`https://api.fhict.nl/schedule/me`, {
      headers: { Authorization: `bearer ${this.authService.getAccessToken()}` },
    })
    return request.data
  }
  async getBuildings() {
    console.log(await this.authService.getAccessToken())
    const request = await axios.default.get(`https://api.fhict.nl/schedule/weeks`, {
      headers: { Authorization: `bearer ${this.authService.getAccessToken()}` },
    })
    return request.data
  }
}
