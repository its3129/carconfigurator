import React from 'react'
import AuthCallback from './views/pages/auth/Callback'
import Company from './views/pages/companies/Company'
import Department from './views/pages/companies/departments/Department'
import FHICTRoster from './views/pages/fhict/Roster'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const Companies = React.lazy(() => import('./views/pages/companies/Companies'))
const Departments = React.lazy(() => import('./views/pages/companies/departments/Departments'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/companies', name: 'Companies', component: Companies },
  { path: '/company/:id', name: 'Company', component: Company },
  { path: '/department/:id', name: 'Department', component: Department },
  { path: '/configurator', name: 'Configurator', component: Companies },
  { path: '/callback', name: 'AuthCallback', component: AuthCallback },
  { path: '/fhict', name: 'fhictRoster', component: FHICTRoster },
  { path: '/departments', name: 'Departments', component: Departments },
]

export default routes
