import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="https://carconfiguratorrozi.com" target="_blank" rel="noopener noreferrer">
          Rozalushka
        </a>
        <span className="ms-1">&copy; 2021 Rozalinka.</span>
      </div>
      <div className="ms-auto">
        <span className="me-1">Powered by</span>
        <a href="https://carconfiguratorrozi.com" target="_blank" rel="noopener noreferrer">
          Rozalushka
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
