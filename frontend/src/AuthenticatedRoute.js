/* eslint-disable react/react-in-jsx-scope */
import { BrowserRouter, Route, Redirect } from 'react-router-dom'

// eslint-disable-next-line react/prop-types
export function AuthenticatedRoute({ component: C, appProps, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        // eslint-disable-next-line react/prop-types
        true ? (
          <C {...props} {...appProps} />
        ) : (
          (window.location.href =
            'https://identity.fhict.nl/connect/authorize?client_id=i384613-bluecableb&scope=fhict%20fhict_personal%20openid%20profile%20email%20roles&redirect_uri=http://localhost:3005/callback&response_type=code')
        )
      }
    />
  )
}
