import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { useState, useEffect } from 'react'
import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'

const ENDPOINT = 'http://localhost:8080/ws'

const Register = () => {
  const [stompClient, setStompClient] = useState(null)
  const [msgToSend, setSendMessage] = useState('Enter your message here!')

  useEffect(() => {
    // use SockJS as the websocket client
    const socket = SockJS(ENDPOINT)
    // Set stomp to use websockets
    const stompClient = Stomp.over(socket)
    // connect to the backend
    stompClient.connect({}, () => {
      // subscribe to the backend
      stompClient.subscribe('/topic/notifications', (data) => {
        console.log(data)
        onMessageReceived(data)
      })
    })
    // maintain the client for sending and receiving
    setStompClient(stompClient)
  }, [])

  // send the data using Stomp
  function sendMessage() {
    stompClient.send('/app/notifications', {}, JSON.stringify({ name: msgToSend }))
  }

  // display the received data
  function onMessageReceived(data) {
    // const result = JSON.parse(data.body)
    // alert(result.content)
  }
  return (
    <div className="Register">
      <br></br>
      <input onChange={(event) => setSendMessage(event.target.value)}></input>
      <button onClick={sendMessage}>Send Message</button>
    </div>

    // <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
    //   <CContainer>
    //     <CRow className="justify-content-center">
    //       <CCol md={9} lg={7} xl={6}>
    //         <CCard className="mx-4">
    //           <CCardBody className="p-4">
    //             <CForm>
    //               <h1>Register</h1>
    //               <p className="text-medium-emphasis">Create your account</p>
    //               <CInputGroup className="mb-3">
    //                 <CInputGroupText>
    //                   <CIcon icon={cilUser} />
    //                 </CInputGroupText>
    //                 <CFormInput placeholder="Username" autoComplete="username" />
    //               </CInputGroup>
    //               <CInputGroup className="mb-3">
    //                 <CInputGroupText>@</CInputGroupText>
    //                 <CFormInput placeholder="Email" autoComplete="email" />
    //               </CInputGroup>
    //               <CInputGroup className="mb-3">
    //                 <CInputGroupText>
    //                   <CIcon icon={cilLockLocked} />
    //                 </CInputGroupText>
    //                 <CFormInput
    //                   type="password"
    //                   placeholder="Password"
    //                   autoComplete="new-password"
    //                 />
    //               </CInputGroup>
    //               <CInputGroup className="mb-4">
    //                 <CInputGroupText>
    //                   <CIcon icon={cilLockLocked} />
    //                 </CInputGroupText>
    //                 <CFormInput
    //                   type="password"
    //                   placeholder="Repeat password"
    //                   autoComplete="new-password"
    //                 />
    //               </CInputGroup>
    //               <div className="d-grid">
    //                 <CButton color="success">Create Account</CButton>
    //               </div>
    //             </CForm>
    //           </CCardBody>
    //         </CCard>
    //       </CCol>
    //     </CRow>
    //   </CContainer>
    // </div>
  )
}

export default Register
