import React from 'react'
import {
  CSpinner,
  CCard,
  CCardImage,
  CCardText,
  CCardTitle,
  CCardBody,
  CButton,
  CRow,
  CCol,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { CDropdown, CDropdownItem, CDropdownMenu } from '@coreui/react'
import { cilOptions } from '@coreui/icons'
import { CDropdownToggle } from '@coreui/react'
import { CompanyService } from '../../../services/Company.service'
import CreateCompany from './CreateCompany'
import Company from './Company'
import { useHistory, useParams } from 'react-router-dom'
import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'

const ENDPOINT = 'http://localhost:8080/ws'

class Companies extends React.Component {
  companyService = null
  socket = null
  stompClient = null
  companies = []

  constructor(props) {
    super(props)
    console.log('test')
    this.socket = SockJS(ENDPOINT)
    const stompClient = Stomp.over(this.socket)

    stompClient.connect({}, () => {
      // subscribe to the backend
      stompClient.subscribe('/topic/notifications', (data) => {
        const result = JSON.parse(data.body)
        alert(result.message.message)
        // alert(data)
        // console.log(data))
      })
    })
    // maintain the client for sending and receiving
    this.stompClient = stompClient
    this.companyService = new CompanyService()
    this.state = { showModal: false }
    this.handleClose = this.handleClose.bind(this)
  }

  handleOpen() {
    this.setState({ showModal: true })
  }
  async handleClose() {
    this.setState({
      companies: await this.companyService.getCompanies(),
      showModal: false,
    })
  }
  async deleteCompany(companyId) {
    await this.companyService.deleteCompany(companyId)
    this.setState({
      companies: await this.companyService.getCompanies(),
    })
  }
  routeToCompany(info) {
    // eslint-disable-next-line react/prop-types
    this.props.history.push(`company/${info.id}`)
  }
  componentDidMount = async () => {
    //console.log(this.FHICTService.getBuildings())
    this.setState({
      showModal: false,
      companies: await this.companyService.getCompanies(),
    })
    console.log(this.state)
  }
  render() {
    return this.state && this.state.companies ? (
      // eslint-disable-next-line react/jsx-key
      <div>
        <CreateCompany visible={this.state.showModal} close={this.handleClose} />
        <CRow xs={{ cols: 1 }} md={{ cols: 4, gutter: 2 }} className="g-4">
          {this.state.companies.map((x) => (
            <div key={x.id}>
              <CCol xs>
                <CCard style={{ height: '15rem' }}>
                  <CDropdown alignment="end">
                    <CDropdownToggle color="secondary" caret={false} className="p-0">
                      <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
                    </CDropdownToggle>
                    <CDropdownMenu>
                      <CDropdownItem onClick={() => this.routeToCompany(x)}>Open</CDropdownItem>
                      <CDropdownItem>Edit</CDropdownItem>
                      <CDropdownItem onClick={() => this.deleteCompany(x.id)}>Delete</CDropdownItem>
                    </CDropdownMenu>
                  </CDropdown>
                  <CCardImage
                    orientation="top"
                    src={x.imageUrl}
                    style={{ maxWidth: '100px', minWidth: '100px' }}
                  />
                  <CCardBody>
                    <CCardTitle>{x.name}</CCardTitle>
                    <CCardText>
                      {x.addressStreet} <br />
                      {x.postalCodeNumbers} {x.postalCodeLetters}
                    </CCardText>
                    <CButton color="secondary" onClick={() => this.routeToCompany(x)}>
                      Company Info
                    </CButton>
                  </CCardBody>
                </CCard>
              </CCol>
            </div>
          ))}
          <CCol xs>
            <CCard className="text-center" style={{ height: '8rem' }}>
              <CCardBody>
                <div className="d-grid gap-2 col-6 mx-auto">
                  <CButton color="primary" size="lg" onClick={() => this.handleOpen()}>
                    Create new Company
                  </CButton>
                </div>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </div>
    ) : (
      <CSpinner color="primary" />
    )
  }
}
export default Companies
