/* eslint-disable react/jsx-no-undef */
import React from 'react'
import {
  CSpinner,
  CCard,
  CCardImage,
  CCardText,
  CCardTitle,
  CCardBody,
  CButton,
  CRow,
  CCol,
} from '@coreui/react'
import { CDropdown, CDropdownItem, CDropdownMenu } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilOptions } from '@coreui/icons'
import { CDropdownToggle } from '@coreui/react'
import { CompanyService } from 'src/services/Company.service'
import { DepartmentService } from 'src/services/Department.service'
import CreateDepartment from './departments/CreateDepartment'
class Company extends React.Component {
  companyId = null
  companies = []
  companyService = null
  constructor(props) {
    // console.log(props)
    super()
    // eslint-disable-next-line react/prop-types
    this.companyId = props.match.params.id
    this.companyService = new CompanyService()
    this.departmentService = new DepartmentService()
    this.state = { showModal: false }
    this.handleClose = this.handleClose.bind(this)
  }
  handleOpen() {
    this.setState({ showModal: true })
  }
  async handleClose() {
    this.setState({
      company: await this.companyService.getCompany(this.companyId),
      showModal: false,
    })
  }
  async deleteDepartment(departmentId) {
    await this.departmentService.deleteDepartment(departmentId)
    this.setState({
      company: await this.companyService.getCompany(this.companyId),
    })
  }
  routeToDeaprtment(info) {
    // eslint-disable-next-line react/prop-types
    this.props.history.push(`/department/${info.id}`)
  }
  componentDidMount = async () => {
    this.setState({
      company: await this.companyService.getCompany(this.companyId),
    })
  }
  render() {
    return this.state && this.state.company ? (
      // eslint-disable-next-line react/jsx-no-comment-textnodes
      <div>
        <CreateDepartment visible={this.state.showModal} close={this.handleClose} />
        <CRow xs={{ cols: 1 }} md={{ cols: 4, gutter: 2 }} className="g-4">
          {this.state.company.departments.map((x) => (
            <div key={x.id}>
              <CCol xs>
                <CCard style={{ height: '15rem' }}>
                  <CDropdown alignment="end">
                    <CDropdownToggle color="secondary" caret={false} className="p-0">
                      <CIcon icon={cilOptions} className="text-high-emphasis-inverse" />
                    </CDropdownToggle>
                    <CDropdownMenu>
                      <CDropdownItem disabled onClick={() => this.handleOpen()}>
                        Open
                      </CDropdownItem>
                      <CDropdownItem>Edit</CDropdownItem>
                      <CDropdownItem onClick={() => this.deleteDepartment(x.id)}>
                        Delete
                      </CDropdownItem>
                    </CDropdownMenu>
                  </CDropdown>
                  <CCardBody>
                    <CCardTitle>Department: {x.name}</CCardTitle>
                    <CButton disabled onClick={() => this.routeToDeaprtment(x)}>
                      Department Info
                    </CButton>
                  </CCardBody>
                </CCard>
              </CCol>
            </div>
          ))}
          <CCol xs>
            <CCard className="text-center" style={{ height: '8rem' }}>
              <CCardBody>
                <div className="d-grid gap-2 col-6 mx-auto">
                  <CButton color="primary" size="lg" onClick={() => this.handleOpen()}>
                    Create new Department
                  </CButton>
                </div>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </div>
    ) : (
      <h1>Loading</h1>
    )
  }
}
export default Company
