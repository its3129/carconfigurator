import React from 'react'
import {
  CSpinner,
  CCard,
  CCardImage,
  CCardText,
  CCardTitle,
  CCardBody,
  CButton,
  CRow,
  CCol,
} from '@coreui/react'
import { DepartmentService } from 'src/services/Department.service'
class Department extends React.Component {
  departmentId = null
  department = null
  departmentService = null
  constructor(props) {
    // console.log(props)
    super()
    // eslint-disable-next-line react/prop-types
    this.departmentId = props.match.params.id
    this.departmentService = new DepartmentService()
    this.state = { showModal: false }
  }
  componentDidMount = async () => {
    this.setState({
      department: await this.departmentService.getDepartment(this.departmentId),
    })
  }
  render() {
    return this.state && this.state.department ? (
      <div>
        <CRow xs={{ cols: 1 }} md={{ cols: 4, gutter: 2 }} className="g-4">
          {this.state.department.getDepartment((x) => (
            <div key={x.id}>
              <CCol xs>
                <CCard style={{ height: '15rem' }}>
                  <CCardBody>
                    <CCardTitle>Department: {x.name}</CCardTitle>
                    {/* <CButton onClick={() => this.routeToDeaprtment(x)}>Department Info</CButton> */}
                  </CCardBody>
                </CCard>
              </CCol>
            </div>
          ))}
        </CRow>
      </div>
    ) : (
      <h1>Loading</h1>
    )
  }
}
export default Department
