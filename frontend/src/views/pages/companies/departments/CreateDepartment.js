/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import { CForm, CFormLabel, CFormInput, CInputGroup, CInputGroupText } from '@coreui/react'
import { CCol, CRow } from '@coreui/react'
import { CModal, CModalBody, CModalHeader, CModalFooter, CModalTitle, CButton } from '@coreui/react'
import axios from 'axios'
import { useHistory } from 'react-router'
import queryString from 'query-string'
import { useParams } from 'react-router'

// eslint-disable-next-line react/prop-types
const CreateDepartment = (props) => {
  const [name, setName] = useState('')
  const { id } = useParams()
  const history = useHistory()

  const handleSubmit = (e) => {
    e.preventDefault()
    // eslint-disable-next-line no-undef
    const department = { name, id }

    // eslint-disable-next-line no-unused-expressions
    fetch('http://localhost:3005/departments', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(department),
    }).then(() => {
      console.log('new department added')
      props.close()
    })
  }
  return (
    <>
      <CModal alignment="center" scrollable visible={props.visible}>
        <CModalHeader onClick={() => props.close()}>
          <CModalTitle>Create a new department</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm className="row g-3">
            <CCol md={12}>
              <CFormLabel htmlFor="inputEmail4">Name</CFormLabel>
              <CFormInput
                type="name"
                id="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </CCol>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => props.close()}>
            Close
          </CButton>
          <CButton color="primary" onClick={handleSubmit} method="post">
            Create department
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default CreateDepartment
