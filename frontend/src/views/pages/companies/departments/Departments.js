import React from 'react'
import {
  CSpinner,
  CCard,
  CCardImage,
  CCardText,
  CCardTitle,
  CCardBody,
  CButton,
  CRow,
  CCol,
} from '@coreui/react'
import { DepartmentService } from 'src/services/Department.service'
// import { DepartmentService } from '../../../services/Department.service'
import { useHistory } from 'react-router'

class Departments extends React.Component {
  departmentService = null
  departments = []

  constructor(props) {
    super(props)
    this.departmentService = new DepartmentService()
  }
  async handleClose() {
    this.setState({
      companies: await this.departmentService.getDepartments(),
      showModal: false,
    })
  }
  routeToCompany(info) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    // const hi = useHistory()
    // eslint-disable-next-line react/prop-types
    // this.context.router.transitionTo()
    // eslint-disable-next-line react/prop-types
    this.props.history.push(`department/${info.id}`)
    console.log(this)
    // this.props.router.push()
  }
  componentDidMount = async () => {
    this.setState({
      departments: await this.departmentService.getDepartments(),
      //testDepartment: await this.departmentService.getDepartment(1),
    })
  }
  render() {
    return this.state && this.state.departments ? (
      // eslint-disable-next-line react/jsx-key
      <CRow xs={{ cols: 1 }} md={{ cols: 4, gutter: 2 }} className="g-4">
        {this.state.departments.map((x) => (
          <div key={x.id}>
            <CCol xs>
              <CCard style={{ height: '15rem' }}>
                <CCardBody>
                  <CCardTitle>{x.name}</CCardTitle>
                  {/* <CButton onClick={() => this.routeToCompany(x)}>Company Info</CButton> */}
                </CCardBody>
              </CCard>
            </CCol>
          </div>
        ))}
        <CCol xs>
          <CCard className="text-center" style={{ height: '8rem' }}>
            <CCardBody>
              <div className="d-grid gap-2 col-6 mx-auto">
                <CButton color="primary" size="lg" onClick={() => this.handleOpen()}>
                  Create new Department
                </CButton>
              </div>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    ) : (
      <CSpinner color="primary" />
    )
    // return this.state.companies !== undefined ? <h1>yay</h1> : <h1>Not loaded</h1>
  }
}
export default Departments
