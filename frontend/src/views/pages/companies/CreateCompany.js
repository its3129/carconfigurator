/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import { CForm, CFormLabel, CFormInput, CInputGroup, CInputGroupText } from '@coreui/react'
import { CCol, CRow } from '@coreui/react'
import { CModal, CModalBody, CModalHeader, CModalFooter, CModalTitle, CButton } from '@coreui/react'
import axios from 'axios'
import { useHistory } from 'react-router'
import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'

const ENDPOINT = 'http://localhost:8080/ws'

// eslint-disable-next-line react/prop-types
const CreateCompany = (props) => {
  const [name, setName] = useState('')
  const [streetName, setStreetName] = useState('')
  const [postalCodeNumbers, setPostalCodeNumbers] = useState('')
  const [postalCodeLetters, setPostalCodeLetters] = useState('')
  const [imageUrl, setimgURl] = useState('')
  const history = useHistory()

  const [stompClient, setStompClient] = useState(null)

  useEffect(() => {
    // use SockJS as the websocket client
    const socket = SockJS(ENDPOINT)
    // Set stomp to use websockets
    const stompClient = Stomp.over(socket)
    // connect to the backend
    stompClient.connect({}, () => {})
    // maintain the client for sending and receiving
    setStompClient(stompClient)
  }, [])

  function sendMessage(message) {
    stompClient.send('/topic/notifications', {}, JSON.stringify({ message: message }))
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    // eslint-disable-next-line no-undef
    const company = { name, streetName, postalCodeNumbers, postalCodeLetters, imageUrl }

    //console.log(company)
    // eslint-disable-next-line no-unused-expressions
    fetch('http://localhost:3005/companies', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(company),
    }).then(() => {
      sendMessage({ message: `Company ${name} succesfully created` })
      props.close()
    })
  }
  return (
    <>
      <CModal alignment="center" scrollable visible={props.visible}>
        <CModalHeader onClick={() => props.close()}>
          <CModalTitle>Create a new company</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm className="row g-3">
            <CCol md={12}>
              <CFormLabel htmlFor="inputEmail4">Name</CFormLabel>
              <CFormInput
                type="name"
                id="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </CCol>
            <CCol xs={12}>
              <CFormLabel htmlFor="inputAddress">Street name</CFormLabel>
              <CFormInput
                id="inputAddress"
                placeholder="Rachelsmolen 10"
                value={streetName}
                onChange={(e) => setStreetName(e.target.value)}
              />
            </CCol>
            <CCol md={6}>
              <CFormLabel htmlFor="inputCity">Postal code numbers</CFormLabel>
              <CFormInput
                id="inputCity"
                placeholder="5642"
                value={postalCodeNumbers}
                onChange={(e) => setPostalCodeNumbers(e.target.value)}
              />
            </CCol>
            <CCol md={6}>
              <CFormLabel htmlFor="inputZip">Postal code letters</CFormLabel>
              <CFormInput
                id="inputZip"
                placeholder="MD"
                value={postalCodeLetters}
                onChange={(e) => setPostalCodeLetters(e.target.value)}
              />
            </CCol>
            <CCol xs={12}>
              <CFormLabel htmlFor="inputUrl">Company logo image URL</CFormLabel>
              <CFormInput
                id="inputUrl"
                placeholder="https://fontys.nl/upload/image648.png"
                value={imageUrl}
                onChange={(e) => setimgURl(e.target.value)}
              />
            </CCol>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => props.close()}>
            Close
          </CButton>
          <CButton color="primary" onClick={handleSubmit} method="post">
            Create company
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default CreateCompany
