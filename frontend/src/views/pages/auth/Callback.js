import React from 'react'
import { AuthService } from 'src/services/Auth.service'
import { Redirect } from 'react-router-dom'
import { cibCoreui } from '@coreui/icons'

class AuthCallback extends React.Component {
  authService = null

  constructor() {
    super()
    this.authService = new AuthService()
  }
  routeToDepartments(info) {
    console.log(info)
  }
  componentDidMount = async () => {
    const urlParams = new URLSearchParams(window.location.search)
    const accessToken = urlParams.get('accessToken')
    const refreshToken = urlParams.get('refreshToken')
    //console.log(urlParams)
    this.authService.setTokens(accessToken, refreshToken)
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/')
  }
  render() {
    // return <Redirect to="/" />
    return <h1>Setting credentials</h1>
    // return this.state.companies !== undefined ? <h1>yay</h1> : <h1>Not loaded</h1>
  }
}
export default AuthCallback
