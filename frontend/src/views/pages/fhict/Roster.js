import React from 'react'
import {
  CSpinner,
  CCard,
  CCardImage,
  CCardText,
  CCardTitle,
  CCardBody,
  CButton,
} from '@coreui/react'
import { FHICTRosterService } from 'src/services/fhict/FHICTRoster.service'

class FHICTRoster extends React.Component {
  FHICTService = null
  rosterInfo = []

  constructor() {
    super()
    this.FHICTService = new FHICTRosterService()
  }
  routeToDepartments(info) {
    console.log(info)
  }
  componentDidMount = async () => {
    this.setState({
      rosterInfo: await this.FHICTService.getRoster(),
    })
    console.log(await this.FHICTService.getRoster())

    // console.log(this.rosterInfo)
  }
  render() {
    return this.state && this.state.rosterInfo ? (
      // eslint-disable-next-line react/jsx-key
      this.state.rosterInfo.data.map((x) => (
        <div key={x.title}>
          <CCard className="h-20">
            <CCardBody>
              <CCardTitle>{x.subject}</CCardTitle>
              <CCardText>
                Starts: {x.start} <br />
                Ends: {x.end}
              </CCardText>
            </CCardBody>
          </CCard>
        </div>
      ))
    ) : (
      <CSpinner color="primary" />
    )
  }
}
export default FHICTRoster
