/* eslint-disable cypress/no-unnecessary-waiting */
describe('My First Test', () => {
  beforeEach(() => {
    //Fake application logged in
    window.localStorage.setItem('access_token', 'test')
  })
  it('Should automatically redirect to dashboard', () => {
    cy.visit('/')
    cy.url().should('eq', 'http://localhost:3000/dashboard')
  })

  it('Should visit /companies', () => {
    cy.visit('/companies')
    cy.intercept('GET', 'http://localhost:3005/companies', { fixture: 'companies.json' }).as(
      'getCompanies',
    )
    cy.wait('@getCompanies')

    cy.url().should('include', 'companies') // => true
    // cy.visit('/dashboard')
  })

  it('Should open company creation modal', () => {
    cy.contains('Create new Company').parent().click()
  })

  it('Should create a new company', () => {
    cy.wait(2000)
    cy.get('#name').type('TestCompany')
    cy.get('#inputAddress').type('TestAddress')
    cy.get('#inputCity').type('1234')
    cy.get('#inputZip').type('AA')
    cy.get('#inputUrl').type('https://fontys.nl/upload/image648.png')
    cy.contains('Create company').parent().click()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.intercept('GET', 'http://localhost:3005/companies', { fixture: 'companies.json' }).as(
      'getCompanies',
    )
    cy.wait('@getCompanies')
    // cy.get('input[name=name]').type('********')
  })

  it('Should automatically redirect to dashboard', () => {
    cy.visit('/')
    cy.url().should('eq', 'http://localhost:3000/dashboard')
  })

  // it('Should close company creation modal', () => {
  //   cy.get('*[class^="btn btn-close"]').click()
  //   cy.get('*[class^="modal fade show d-block enter-done"]').should('not.exist')
  // })

  it('Should visit /departments', () => {
    cy.visit('/departments')

    cy.intercept('GET', 'http://localhost:3005/departments', { fixture: 'departments.json' }).as(
      'getDepartments',
    )
    cy.wait('@getDepartments')

    cy.contains('Departments').parent().click()
    cy.url().should('include', 'departments') // => true
  })
})
