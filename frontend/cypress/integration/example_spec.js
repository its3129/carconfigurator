describe('My First Test', () => {
  beforeEach(() => {
      //Fake application logged in
    window.localStorage.setItem('access_token', 'test')
  })
  it('Should automatically redirect to dashboard', () => {
    cy.visit('/')
    cy.url().should('eq', 'http://localhost:3000/dashboard')
  })

  it('Should visit /dashboard', () => {
    cy.visit('/dashboard')
  })
})
